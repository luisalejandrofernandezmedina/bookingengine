﻿using BookingEngine.UseCasePorts.Hotel;
using BookingEngine.UseCases.CreateHotel;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.UseCases
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddUseCasesServices(this IServiceCollection services)
        {
            services.AddTransient<ICreateHotelInputPort, CreateHotelInteractor>();
            services.AddTransient<IGetAllHotelInputPort, GetAllHotelInteractor>();
            return services;
        }
    }
}
