﻿using AutoMapper;
using BookingEngine.DTOs;
using BookingEngine.Entities.Interface;
using BookingEngine.UseCasePorts.Hotel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.UseCases.CreateHotel
{
    public class GetAllHotelInteractor : IGetAllHotelInputPort
    {
        readonly IUnitOfWork UnitOfWork;
        readonly IGetAllHotelOuputPort OutputPort;
        public GetAllHotelInteractor(
            IUnitOfWork unitOfWork,
            IGetAllHotelOuputPort outputPort) =>
            (UnitOfWork, OutputPort) = (unitOfWork, outputPort);
        public async Task Handle()
        {
            var res = UnitOfWork._hotelRepository.GetHotels().Select( h =>
                    new HotelDto
                    {
                        Name = h.Name,
                        Address = h.Address,
                        City = h.City.ToString(), 
                        Starts = h.Starts
                    }
                ).ToList();
            await OutputPort.Handle(res);
        }
    }
}
