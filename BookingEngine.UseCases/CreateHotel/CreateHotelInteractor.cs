﻿using BookingEngine.DTOs;
using BookingEngine.Entities.Interface;
using BookingEngine.Entities.POCOs;
using BookingEngine.UseCasePorts;
using BookingEngine.UseCasePorts.Hotel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.UseCases.CreateHotel
{
    public class CreateHotelInteractor : ICreateHotelInputPort
    {

        readonly IUnitOfWork UnitOfWork;
        readonly ICreateHotelOutputPort OutputPort;

        public CreateHotelInteractor(
            IUnitOfWork unitOfWork,
            ICreateHotelOutputPort outputPort) =>
            (UnitOfWork, OutputPort) = (unitOfWork, outputPort);

        public async Task Handle(CreateHotelDto hotel)
        {
                var res =  UnitOfWork._hotelRepository.CreateHotel(new Hotel
                 {
                     Name = hotel.Name,
                     Address = hotel.Address,
                     City = (City)hotel.City,
                     Starts = hotel.Starts
                 });
                await OutputPort.Handle(new NewHotelDto { Id = res, Name = hotel.Name});
        }
    }
}
