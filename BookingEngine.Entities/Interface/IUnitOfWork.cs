﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.Interface
{
    public interface IUnitOfWork
    {
        IHotelRepository _hotelRepository { get; }
    }
}
