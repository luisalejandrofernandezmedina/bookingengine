﻿using BookingEngine.Entities.POCOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.Interface
{
    public interface IHotelRepository
    {
        Guid CreateHotel(Hotel hotel);
        List<Hotel> GetHotels();
        int UpdteStatusHotel(Guid id);
    }
}
