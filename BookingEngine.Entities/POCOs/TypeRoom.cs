﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.POCOs
{
    public class TypeRoom
    {
        public int Id { get; set; }
        public int Name { get; set; }
        public string Description { get; set; }
    }
}
