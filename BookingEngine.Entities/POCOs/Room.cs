﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.POCOs
{
    public class Room
    {
        public Guid Id { get; set; }
        public decimal BaseCost { get; set; }
        public Hotel Hotel { get; set; }
        public TypeRoom Type { get; set; }
        public List<Task> Task { get; set; }
        public int Bed { get; set; }    
        public bool Status { get; set; }
    }
}
