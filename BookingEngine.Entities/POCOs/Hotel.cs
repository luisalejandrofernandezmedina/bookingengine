﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.POCOs
{
    public class Hotel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int Starts { get; set; }
        public City City { get; set; }
    }

    public enum City
    {
        Bogota = 1,
        Medellin = 2,
        Cali = 3,
        Barranquilla = 4
    }
}
