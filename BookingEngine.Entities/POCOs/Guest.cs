﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.POCOs
{
    public class Guest
    {
        public int Document { get; set; }
        public string Names { get; set; }
        public string Surnames { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
    }

    public enum Gender
    {
        Masculino = 1,
        Femenino = 2 
    }
}
