﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Entities.POCOs
{
    public class Agency
    {
        public Guid Id { get; set; }
        public string Nit { get; set; }
        public string Name { get; set; }
    }
}
