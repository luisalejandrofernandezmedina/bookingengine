﻿using BookingEngine.DTOs;
using BookingEngine.Presenters;
using BookingEngine.UseCasePorts.Hotel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BookingEngine.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreateHotelController
    {
        readonly ICreateHotelInputPort InputPort;
        readonly ICreateHotelOutputPort OutputPort;

        public CreateHotelController(ICreateHotelInputPort inputPort,
            ICreateHotelOutputPort outputPort) =>
            (InputPort, OutputPort) = (inputPort, outputPort);

        [HttpPost]
        public async Task<NewHotelDto> CreateHotel([FromBody] CreateHotelDto hotelDto)
        {
            await InputPort.Handle(hotelDto);
            return ((IPresenter<NewHotelDto>)OutputPort).Content;
        }

    }
}
