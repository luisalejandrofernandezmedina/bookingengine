﻿using BookingEngine.DTOs;
using BookingEngine.Presenters;
using BookingEngine.UseCasePorts.Hotel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetAllHotelController
    {

        readonly IGetAllHotelInputPort InputPort;
        readonly IGetAllHotelOuputPort OutputPort;

        public GetAllHotelController(IGetAllHotelInputPort inputPort,
            IGetAllHotelOuputPort outputPort) =>
            (InputPort, OutputPort) = (inputPort, outputPort);

        [HttpGet]
        public async Task<List<HotelDto>>GetAllHotel()
        {
            await InputPort.Handle();
            return ((IPresenter<List<HotelDto>>)OutputPort).Content;
        }
    }
}
