﻿using BookingEngine.Entities.Interface;
using BookingEngine.Entities.POCOs;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.RepositoryDapper.Repositories
{
    public class HotelRepository : IHotelRepository
    {
        public Guid CreateHotel(Hotel hotel)
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["BookingConnect"].ToString()))
            {
                var query = "USP_CreateHotel";
                var res = connect.ExecuteScalar<Guid>(query
                      , new
                      {
                          NAME = hotel.Name
                      ,
                          ADDRESS = hotel.Address
                      ,
                          CITY = hotel.City
                      ,
                          STARTS = hotel.Starts
                      }, commandType: CommandType.StoredProcedure);
                return res;
            }

        }

        public List<Hotel> GetHotels()
        {
            using (var connect = new SqlConnection(ConfigurationManager.AppSettings["BookingConnect"].ToString()))
            {
                var query = "USP_GetHotels";
                var res = connect.Query<Hotel>(query, commandType: CommandType.StoredProcedure).ToList();
                return res;
            }
        }

        public int UpdteStatusHotel(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
