﻿using BookingEngine.Entities.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.RepositoryDapper.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IHotelRepository hotelRepository)
        {
            _hotelRepository = hotelRepository;
        }

        public IHotelRepository _hotelRepository { get; }
}
}
