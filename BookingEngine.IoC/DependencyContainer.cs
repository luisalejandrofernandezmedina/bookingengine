﻿using BookingEngine.Presenters;
using BookingEngine.RepositoryDapper;
using BookingEngine.UseCases;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BookingEngine.IoC
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddBookingEngineDependencies(
            this IServiceCollection services)
        {
            services.AddRepositories();
            services.AddUseCasesServices();
            services.AddPresenters();
            return services;
        }
    }
}
