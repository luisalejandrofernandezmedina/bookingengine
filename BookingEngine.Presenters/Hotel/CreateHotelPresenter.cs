﻿using BookingEngine.DTOs;
using BookingEngine.UseCasePorts.Hotel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Presenters.Hotel
{
    public class CreateHotelPresenter : ICreateHotelOutputPort, IPresenter<NewHotelDto>
    {
        public NewHotelDto Content { get; private set; }

        public Task Handle(NewHotelDto hotel)
        {
            Content = hotel;
            return Task.CompletedTask;
        }
    }
}
