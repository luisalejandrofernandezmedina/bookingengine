﻿using BookingEngine.DTOs;
using BookingEngine.UseCasePorts.Hotel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Presenters.Hotel
{
    public class GetAllHotelPresenter : IGetAllHotelOuputPort, IPresenter<List<HotelDto>>
    {
        public List<HotelDto> Content { get; private set; }

        public Task Handle(List<HotelDto> hotels)
        {
            Content = hotels;
            return Task.CompletedTask;
        }
    }
}
