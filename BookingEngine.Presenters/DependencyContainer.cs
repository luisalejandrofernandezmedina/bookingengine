﻿using BookingEngine.Presenters.Hotel;
using BookingEngine.UseCasePorts.Hotel;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.Presenters
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddPresenters(this IServiceCollection services)
        {
            services.AddScoped<ICreateHotelOutputPort, CreateHotelPresenter>();
            services.AddScoped<IGetAllHotelOuputPort, GetAllHotelPresenter>();

            return services;
        }
    }
}
