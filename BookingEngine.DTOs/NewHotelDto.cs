﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.DTOs
{
    public class NewHotelDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
