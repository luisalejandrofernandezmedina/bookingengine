﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.DTOs
{
    public class HotelDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Starts { get; set; }
        public string City { get; set; }
    }
}
