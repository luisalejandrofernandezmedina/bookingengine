﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.UseCasePorts.Hotel
{
    public interface IGetAllHotelInputPort
    {
        Task Handle();
    }
}
