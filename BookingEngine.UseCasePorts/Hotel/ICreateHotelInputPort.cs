﻿using BookingEngine.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookingEngine.UseCasePorts.Hotel
{
    public interface ICreateHotelInputPort
    {
        Task Handle(CreateHotelDto hotel);
    }
}
